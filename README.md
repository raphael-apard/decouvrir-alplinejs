# Découvrir AlplineJS


## Travail à faire 

En utilisant Alpline JS

* afficher et masquer le menu mobile on clique sur le burger menu
* afficher et masquer le menu utilisateur on clique sur l'icone de l'utilisateur
* récupérer et afficher la liste des utilisateurs disponible ici : https://jsonplaceholder.typicode.com/users
* filtrer la liste des utilisateurs lors de la saisie dans le champs de recherche (avec un debounce de 500ms)
* trier la liste des utilisateurs au clique sur le nom des colones. Ascendant au premier clic puis descendant au deuxième clic